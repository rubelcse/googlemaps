<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   public function searchLocation()
   {
       return view('maps.search');
   }

    public function index()
    {
        return view('home');
    }

    public function gmaps(){
        $locations = Location::get();
        return view('gmaps',compact('locations'));
    }
}
