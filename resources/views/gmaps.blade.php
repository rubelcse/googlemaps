<!DOCTYPE html>

<html>

<head>

    <title>Laravel 5 - Multiple markers in google map using gmaps.js</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="http://maps.google.com/maps/api/js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>

    {{--<script src="{{asset('map/jquery.min.js')}}"></script>

    <script src="{{asset('map/map.js')}}"></script>

    <script src="{{asset('map/gmaps.js')}}"></script--}}>


    <style type="text/css">

        #mymap {

            border:1px solid red;

            width: 800px;

            height: 500px;

        }

    </style>


</head>

<body>


<h1>Bangladesh farmgate Location</h1>


<div id="mymap"></div>


<script type="text/javascript">


    var locations = <?php print_r(json_encode($locations)) ?>;


    var mymap = new GMaps({

        el: '#mymap',

        lat: 23.7561067,

        lng: 90.3871961,

        zoom:20

    });


    $.each( locations, function( index, value ){

        mymap.addMarker({

            lat: value.lat,

            lng: value.lng,

            title: value.city,

            click: function(e) {

                alert('This is '+value.city+', Dhaka from Bangladesh.');

            }

        });
        mymap.drawOverlay({
            lat: value.lat,

            lng: value.lng,
            content: '<h1 style="background-color: #2ab27b">'+value.city+'</h1>',

        });

    });


</script>


</body>

</html>